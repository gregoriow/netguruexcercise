How to run tests in Cypress?

#Download repository

Download repository from https://bitbucket.org/gregoriow/netguruexcercise/downloads/ 
and unzip it.

#Download and install Cypress 

Download Cypress from from https://download.cypress.io/desktop 
and install it.

#Chrome browser

Make sure that you have installed Chrome browser.

#Run tests in Cypress

1. Run Cypress.exe and select manually unzipped repository as your project.
2. Go to unzipped repository and cut folders /elements, /fixtures and /integration. Next paste it into /cypress folder.
3. Go to back to Cypress and click on 'SignUpTest.spec.js' on INTEGRATION TESTS list.
4. After completion of the test close the browser window and select second test 'LoginTests.spec.js'.