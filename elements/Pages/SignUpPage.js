class SignUpPage {

    visit() {
        cy.visit('https://www.trello.com/en-US');
    }

    SignUpButton() {
        const button =cy.contains('Sign Up').click({force:true});
    }

    FillEmail(value) {
        const field = new cy.get('#email').click({force:true})
        field.type(value, {force:true});
        return this;
    }

    Submit() {
        const button = new cy.get('#signup').click({force:true});
    }

    FillName(value) {
        const field = new cy.get('#name').click({force:true})
        field.type(value, {force:true});
        return this;
    }

    FillPassword(value) {
        const field = new cy.get('#password').click({force:true})
        field.type(value, {force:true});
        return this;
    }

    SignUpAs(email, name, password) {
        this.FillEmail(email);
        this.Submit();
        this.FillName(name);
        this.FillPassword(password);
        this.Submit();
        return this;
    }

    SignUpWithFailName() {
        cy.fixture('users.json').as('user');
        cy.get('@user').then((users) => {
            this.SignUpAs(users.fail_name.email, users.fail_name.name, users.fail_name.password);
        })
    }

}
export default SignUpPage;