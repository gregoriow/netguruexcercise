class LoginPage {

    visit() {
            cy.visit('https://www.trello.com/en-US');
        }

    LoginButton() {
            const button =cy.contains('Log In').click({force : true});
        }

    FillEmail(value) {
        const field = new cy.get('#user').click({force:true})
        field.type(value, {force:true});
        return this;
    }

    FillPassword(value) {
        const field = new cy.get('#password').click({force:true})
        field.type(value, {force:true});
        return this;
    }

    Submit() {
        const button = new cy.get('#login').click({force:true});
    }

    LoginAs(email,password) {
        this.FillEmail(email);
        this.FillPassword(password);
        this.Submit();
        return this;
    }

    LoginAsFailUser() {
        cy.fixture('users.json').as('user');
        cy.get('@user').then((users)=>{
            this.LoginAs(users.fail_user.email, users.fail_user.password);
        })
    }
}
export default LoginPage;