import SignUpPage from '../elements/Pages/SignUpPage.js'

describe('Sign Up Test', function() {

    before(function() {
        cy.clearCookies();
    })

    it('Fail Name', function() {
        const home = new SignUpPage();
        home.visit();
        home.SignUpButton();
        cy.url().should('include', '/signup');

        const signup = new SignUpPage();
        signup.SignUpWithFailName();
        cy.get('.error-message').should('be.visible');
    })

})