import LoginPage from '../elements/Pages/LoginPage.js';

describe('Login Test', function () {

    before(function() {
        cy.clearCookies()
    })

    it('Fail Login', function () {
        const home = new LoginPage();
        home.visit();
        home.LoginButton();
        cy.url().should('include', '/login');

        const login = new LoginPage();
        login.LoginAsFailUser();
        cy.get('.error-message').should('be.visible');
    })

})